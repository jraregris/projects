import gleam/io
import gleam/list
import gleam/result
import gleam/string

pub fn from_file(file: String) -> List(String) {
  file
  |> string.split("[[project]]")
  |> reject(string.is_empty)
  |> list.map(from_string)
  |> result.values()
}

fn from_string(string) -> Result(String, String) {
  let lines =
    string.split(string, "\n")
    |> reject(string.is_empty)
    |> list.map(fn(s) { string.replace(s, each: "\"", with: "") })

  case lines {
    ["name=" <> name, ..] -> Ok(name)
    _ -> Error("wrong")
  }
}

fn reject(list: List(a), rejecting predicate: fn(a) -> Bool) -> List(a) {
  list.filter(list, fn(a) { !predicate(a) })
}
