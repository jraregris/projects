import gleam/io
import gleam/list
import projectsfile

pub fn main(args: List(String)) -> Result(Nil, Nil) {
  case args {
    ["pick"] -> pick()
    ["help"] -> help()
    _ -> Error(Nil)
  }
}

fn pick() -> Result(Nil, Nil) {
  case projectsfile.projects() {
    Ok(projects) ->
      case projects {
        [] -> {
          io.println("No projects")
          Error(Nil)
        }
        list -> {
          let assert Ok(prj) = list.shuffle(list) |> list.first()
          io.println(prj)
          Ok(Nil)
        }
      }
    _ -> todo
  }
}

fn help() -> Result(Nil, Nil) {
  "
  projects <command>

  pick            suggest random project
  list            list projects

  add <project>   add project
  del <project>   delete project

  init            initialise projects file
  help            this text

  if no command is given, <pick>
  "
  |> io.println()
  Ok(Nil)
}
