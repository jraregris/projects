import envoy
import gleam/io
import gleam/result
import project
import simplifile

pub fn projects() -> Result(List(String), String) {
  case simplifile.read(projectsfile()) {
    Ok(file) -> {
      project.from_file(file)
      |> Ok()
    }
    Error(err) -> {
      let error_message =
        "Could not read projectsfile: " <> simplifile.describe_error(err)

      io.println_error(error_message)
      Error(error_message)
    }
  }
}

fn projectsfile() {
  data_dir() <> "/projects/projectsfile"
}

fn data_dir() {
  envoy.get("XDG_DATA_HOME") |> result.unwrap(home() <> "/.local/share")
}

fn home() {
  let assert Ok(home) = envoy.get("HOME")
  home
}
