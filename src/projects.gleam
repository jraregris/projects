import argv
import cli

pub fn main() {
  case cli.main(argv.load().arguments) {
    Ok(Nil) -> Nil
    Error(Nil) -> halt(1)
  }
}

@external(erlang, "erlang", "halt")
@external(javascript, "process", "exit")
fn halt(code: Int) -> Nil
