# PROJECTS

> I have worked so hard to get this computer project going! – Bill Gates

## Development

```sh
gleam run   # Run the project
gleam test  # Run the tests
```

## Build

```sh
gleam run -m  gleescript  # Build escript binary
```

## Install

Either copy `./projects` to somewhere on `$PATH` or link to it from somewhere on `$PATH`.
