defmodule Projects do
  alias Projects.Projectsfile
  alias Projects.Project

  def pick() do
    case Projectsfile.projects() do
      [] -> IO.puts("No projects")
      projects -> projects |> Enum.random() |> print_project()
    end
  end

  def list() do
    Projectsfile.projects() |> print_projects()
  end

  def add([projectname, tasktag]) do
    project = %Project{name: projectname, tasktag: tasktag}

    [project | Projectsfile.projects()]
    |> Projectsfile.write_projects()
  end

  def add([projectname]) when is_binary(projectname) do
    project = %Project{name: projectname}

    [project | Projectsfile.projects()]
    |> Projectsfile.write_projects()
  end

  def del(project) do
    true = Enum.member?(Projectsfile.projects(), project)
    Projectsfile.projects() |> Enum.reject(&(&1 == project)) |> Projectsfile.write_projects()
  end

  def init() do
    init(force: false)
  end

  def init(opts) do
    projectsfile = Projectsfile.projectsfile()

    force = opts[:force]

    if(not force && Projectsfile.exitsts?()) do
      IO.puts("Project file, #{projectsfile} already exist")
    else
      File.write(projectsfile, "")
      IO.puts("Created new project file, #{projectsfile}.")
    end
  end

  defp print_projects(projects) do
    projects |> Enum.map(&project_to_string/1) |> Enum.join("\n") |> IO.puts()
  end

  defp print_project(project) do
    project |> project_to_string() |> IO.puts()
  end

  defp project_to_string(project) do
    "#{project.name}"
  end
end
