defmodule Projects.Project do
  defstruct [:name, :tasktag]
  @type t :: %{name: String.t(), tasktag: String.t()}

  def from_string(string) when is_binary(string) do
    case string |> String.split("\n", trim: true) do
      ["name=" <> name] ->
        %Projects.Project{name: name}

      ["name=" <> name, "tasktag=" <> tag] ->
        %Projects.Project{name: name, tasktag: tag}
    end
  end

  def from_file(string) when is_binary(string) do
    string
    |> String.split("[[project]]", trim: true)
    |> Enum.map(&Projects.Project.from_string/1)
  end

  def to_string(project) do
    ["[[project]]", name_to_string(project), tasktag_to_string(project)] |> Enum.join("\n")
  end

  def tasktag_to_string(project) do
    case project.tasktag do
      nil -> ""
      tag -> "tasktag=#{tag}"
    end
  end

  def name_to_string(project) do
    case project.name do
      nil -> ""
      name -> "name=#{name}"
    end
  end
end
