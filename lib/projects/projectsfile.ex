defmodule Projects.Projectsfile do
  alias Projects.Project

  def projectsfile do
    data_dir() |> Path.join("/projects/projectsfile")
  end

  defp data_dir() do
    System.get_env("XDG_DATA_HOME") || Path.join(System.user_home(), "/.local/share")
  end

  def projects() do
    case File.read(projectsfile()) do
      {:ok, file} ->
        Project.from_file(file)

      {:error, :enoent} ->
        IO.puts("No projects file found!")
        exit({:shutdown, 1})
    end
  end

  def exitsts?() do
    File.exists?(projectsfile())
  end

  def write_projects(projects) do
    :ok =
      File.write!(
        projectsfile(),
        projects |> Enum.map(&Project.to_string/1) |> Enum.join("\n")
      )
  end
end
