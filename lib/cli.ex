defmodule Projects.CLI do
  def main(args \\ []) do
    case args do
      [] -> Projects.pick()
      ["help"] -> help()
      ["pick"] -> Projects.pick()
      ["list"] -> Projects.list()
      ["add" | input] -> Projects.add(input)
      ["del", project] -> Projects.del(project)
      ["init"] -> Projects.init()
      ["init", "--force"] -> Projects.init(force: true)
    end
  end

  defp help() do
    """
    projects <command>

    pick            suggest random project
    list            list projects

    add <project>   add project
    del <project>   delete project

    init            initialise projects file
    help            this text

    if no command is given, <pick>
    """
    |> IO.puts()
  end

end
