# Changelog

Until [1.0.0] this will only hold breaking changes

## [0.2.0] - 2020-11-01

### Changed name and location of projects file

Prior to [0.2.0] the projects file would be `~/.projects`. In [0.2.0] the projects file will be called `projectsfile` and will be located in a `projects` directory in `~/.local/share` unless something else is specified in `$XDG_DATA_HOME`.